<?php

/**
 * @file
 * Provides dynamic panes functionality.
 */

/**
 * Implements hook_menu().
 */
function dynamic_panes_menu() {
  // Module settings.
  $items['admin/config/dynamic-panes'] = array(
    'title' => 'Dynamic panes',
    'description' => 'Dynamic panes tools.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );

  return $items;
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function dynamic_panes_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}

/**
 * Implements hook_ctools_plugin_type().
 */
function dynamic_panes_ctools_plugin_type() {
  $plugins['dynamic_panes_layout_providers'] = array(
    'extension' => 'inc',
    'classes' => array('handler'),
    'cache' => TRUE,
  );
  $plugins['dynamic_panes_layout_handlers'] = array(
    'extension' => 'inc',
    'classes' => array('handler'),
    'cache' => TRUE,
  );
  $plugins['dynamic_panes_context_handlers'] = array(
    'extension' => 'inc',
    'classes' => array('handler'),
    'cache' => TRUE,
  );
  return $plugins;
}

/**
 * Fetch an array of regions.
 */
function dynamic_panes_get_static_regions() {
  $regions = module_invoke_all('dynamic_panes_static_regions_info');
  drupal_alter('dynamic_panes_static_regions_info', $regions);

  return $regions;
}

/**
 * Implements hook_dynamic_panes_static_regions_info().
 */
function dynamic_panes_dynamic_panes_static_regions_info() {
  return array(
    'static_top_first' => t('Static: Top first'),
    'static_top_second' => t('Static: Top second'),
    'static_top_third' => t('Static: Top third'),

    'static_main_first' => t('Static: Main first'),
    'static_main_second' => t('Static: Main second'),
    'static_main_third' => t('Static: Main third'),

    'static_bottom_first' => t('Static: Bottom first'),
    'static_bottom_second' => t('Static: Bottom second'),
    'static_bottom_third' => t('Static: Bottom third'),
  );
}

/**
 * Implements hook_panelizer_pre_render_alter().
 */
function dynamic_panes_panelizer_pre_render_alter(&$panelizer, &$display, &$entity) {
  foreach ($display->content as $key => &$pane) {
    if ($pane->subtype == 'dynamic_panes') {
      $pane->configuration['region'] = $pane->panel;
    }
  }
}

/**
 * Fetch an array of regions.
 */
function dynamic_panes_get_regions($include_static = TRUE, $hierarchy = FALSE) {
  ctools_include('plugins', 'panels');

  $layouts = panels_get_layouts();

  $dynamic_regions = array();

  foreach ($layouts as $layout) {
    if (empty($layout['regions'])) {
      continue;
    }
    $regions = array();
    if ($hierarchy) {
      foreach ($layout['regions'] as $region_id => $layout_region) {
        $regions[$layout['name'] . '-' . $region_id] = $layout_region;
      }
      $dynamic_regions += array(t('Dynamic: Layout: @name', array('@name' => $layout['name'])) => $regions);
    }
    else {
      $regions = $layout['regions'];
      $dynamic_regions += $regions;
    }
  }
  if ($include_static) {
    $static_regions = dynamic_panes_get_static_regions();
    if ($hierarchy) {
      $static_regions = array(t('Static (Same for all layots)') => $static_regions);
    }
  }
  $regions = $dynamic_regions;
  if ($include_static) {
    $regions += $static_regions;
  }
  drupal_alter('dynamic_panes_regions', $regions);

  return $regions;
}

/**
 * Get layout providers.
 *
 * @param array $contexts
 *   An array contains ctools context objects.
 *
 * @return \Drupal\dynamic_panes\LayoutProvider[]
 *   An array contains layout providers.
 */
function dynamic_panes_get_layout_providers($contexts) {
  $providers = array();

  ctools_include('plugins');
  $plugins = ctools_get_plugins('dynamic_panes', 'dynamic_panes_layout_providers');
  foreach ($plugins as $plugin) {
    if ($class = ctools_plugin_get_class($plugin, 'handler')) {
      $reflection = new ReflectionClass($class);
      if ($reflection->isSubclassOf('\Drupal\dynamic_panes\LayoutProvider')) {
        $providers[$plugin['name']] = new $class($contexts);
      }
    }
  }

  return $providers;
}

/**
 * Get layouts.
 */
function dynamic_panes_get_layouts($contexts) {
  $layouts = array();
  foreach (dynamic_panes_get_layout_providers($contexts) as $provider) {
    $layouts += $provider->getLayouts($contexts);
  }

  return $layouts;
}

/**
 * Get pane object for display.
 *
 * @param array $contexts
 *   An array contains ctools context objects.
 * @param array $params
 *   An array contains configs of content type plugin.
 * @param object $display
 *   The panels display.
 *
 * @return \Drupal\dynamic_panes\Pane
 *   The pane object.
 */
function dynamic_panes_get_pane($contexts, $params, $display) {
  $pane_class = '\Drupal\dynamic_panes\Pane';
  if (!empty($display->dynamicPanesPaneClass)) {
    $reflection = new \ReflectionClass($display->dynamicPanesPaneClass);
    if ($reflection->isSubclassOf($pane_class)) {
      $pane_class = $display->dynamicPanesPaneClass;
    }
  }

  return new $pane_class($contexts, $params, $display);
}

/**
 * Renders content for the pane.
 */
function dynamic_panes_get_content($contexts, $params) {
  $content = array();
  $display = panels_get_current_page_display();
  try {
    $pane = dynamic_panes_get_pane($contexts, $params, $display);

    $regions = dynamic_panes_get_regions($display);
    $region_name = $pane->getRegionName();

    if (!isset($regions[$region_name])) {
      return $content;
    }

    $content = $pane->render();

    if (isset($params['pane'])) {
      $params['pane']->configuration['dynamic_panes_pane'] = $pane;
    }
  }
  catch (Exception $e) {
  }

  return $content;
}

/**
 * Implements hook_field_widget_info().
 */
function dynamic_panes_field_widget_info() {
  return array(
    'dynamic_panes_region' => array(
      'label' => t('Dynamic panes: Regions list from panels layout'),
      'field types' => array('text'),
    ),
    'dynamic_panes_ruleset' => array(
      'label' => t('Dynamic panes: Ruleset condition if layout should be used.'),
      'field types' => array('text'),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function dynamic_panes_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $main_widget = array();
  switch ($instance['widget']['type']) {
    case 'dynamic_panes_region':
      $options = dynamic_panes_get_regions(TRUE, TRUE);

      $default_value = isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL;
      if ($region = filter_input(INPUT_GET, 'region', FILTER_SANITIZE_SPECIAL_CHARS)) {
        foreach ($options as $option_group) {
          if (isset($option_group[$region])) {
            $default_value = $region;
            $fixed_value = TRUE;
          }
        }
      }

      if (empty($fixed_value)) {
        $main_widget = $element + array(
          '#type' => 'select',
          '#title' => t('Regions'),
          '#default_value' => $default_value,
          '#options' => $options,
          '#attributes' => array('readonly' => 'readonly'),
          '#empty_option' => t('- Select -'),
        );
      }
      else {
        $main_widget = array(
          '#type' => 'hidden',
          '#value' => $default_value,
        );
      }
      break;

    case 'dynamic_panes_ruleset':
      $options = array();
      ctools_include('context');
      ctools_include('export');
      $rulesets = ctools_export_crud_load_all('ctools_access_ruleset', FALSE);
      foreach ($rulesets as $ruleset) {
        if (empty($ruleset->disabled)) {
          $options[$ruleset->name] = $ruleset->admin_title;
        }
      }

      $main_widget = $element + array(
        '#type' => 'select',
        '#title' => t('Rulesets'),
        '#default_value' => isset($items[$delta]['value']) ? $items[$delta]['value'] : NULL,
        '#options' => $options,
        '#empty_option' => t('- Select -'),
      );
      break;
  }

  $element['value'] = $main_widget;
  return $element;
}

/**
 * Implements hook_permission().
 */
function dynamic_panes_permission() {
  return array(
    'administer dynamic panes' => array(
      'title' => t('Administer dynamic panes'),
      'description' => t('Allows users to administer dynamic panes.'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Get available choices for config "Level" in content type plugin.
 *
 * @return array
 *   An array contains available choices.
 */
function _dynamic_panes_get_config_levels() {
  return drupal_map_assoc(range(1, 10));
}
