<?php

/**
 * @file
 * Plugin for render Panels display as In-place editor.
 */

$plugin = array(
  'renderer' => 'dynamic_panes_ipe_renderer',
);
