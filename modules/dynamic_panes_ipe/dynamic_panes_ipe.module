<?php

/**
 * @file
 * Primarily Drupal hooks.
 *
 * Provides a In-place Editor for Dynamic panes.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function dynamic_panes_ipe_ctools_plugin_directory($module, $plugin) {
  if ($module == 'panels' && $plugin == 'display_renderers') {
    return 'plugins/' . $plugin;
  }
}

/**
 * Implements hook_ctools_plugin_api().
 */
function dynamic_panes_ipe_ctools_plugin_api($module, $api) {
  if ($module == 'panels' && $api == 'pipelines') {
    return array(
      'version' => 1,
      'path' => drupal_get_path('module', 'dynamic_panes_ipe') . '/includes',
    );
  }
}

/**
 * Implements hook_theme().
 */
function dynamic_panes_ipe_theme() {
  return array(
    'dynamic_panes_ipe_region' => array(
      'variables' => array(
        'pane' => NULL,
        'region' => NULL,
        'level' => NULL,
      ),
      'file' => 'theme/theme.inc',
    ),
    'dynamic_panes_ipe_region_toolbar' => array(
      'variables' => array(
        'pane' => NULL,
        'region' => NULL,
        'level' => NULL,
      ),
      'file' => 'theme/theme.inc',
    ),
    'dynamic_panes_ipe_block' => array(
      'variables' => array(
        'block' => NULL,
      ),
      'file' => 'theme/theme.inc',
    ),
    'dynamic_panes_ipe_toolbar' => array(
      'variables' => array(
        'buttons' => array(),
      ),
      'file' => 'theme/theme.inc',
    ),
  );
}

/**
 * Implements hook_page_alter().
 *
 * Adds the IPE control container.
 */
function dynamic_panes_ipe_page_alter(&$page) {
  $buttons = &drupal_static('dynamic_panes_ipe_toolbar_buttons', array());
  if (empty($buttons)) {
    return;
  }

  $output = theme('dynamic_panes_ipe_toolbar', array('buttons' => $buttons));

  $page['page_bottom']['dynamic_panes_ipe'] = array(
    '#markup' => $output,
  );
}

/**
 * Form constructor for the sort blocks form.
 *
 * @see dynamic_panes_ipe_sort_form()
 *
 * @ingroup forms
 */
function dynamic_panes_ipe_sort_form($form, $form_state) {
  $form['#sort_handlers'] = array();

  $elements = drupal_static('dynamic_panes_ipe_toolbar_form', array());
  $form = drupal_array_merge_deep($form, $elements);

  $form['buttons'] = array('#weight' => 100);
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#id' => 'dynamic-panes-ipe-save',
    '#attributes' => array('class' => array('dynamic-panes-ipe-save')),
    '#submit' => array('dynamic_panes_ipe_sort_submit'),
    '#ajax' => array(
      'callback' => 'dynamic_panes_ipe_sort_sort_js',
    ),
  );
  $form['buttons']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#id' => 'dynamic-panes-ipe-cancel',
    '#attributes' => array('class' => array('dynamic-panes-ipe-cancel')),
  );

  return $form;
}

/**
 * Form submission dynamic_panes_ipe_sort_form().
 *
 * @see dynamic_panes_ipe_sort_form()
 *
 * @ingroup forms
 */
function dynamic_panes_ipe_sort_submit($form, $form_state) {
  foreach ($form['#sort_handlers'] as $method) {
    if (is_callable($method)) {
      call_user_func_array($method, array($form, &$form_state));
    }
  }
}

/**
 * Ajax callback in response to a new empty widget being added to the form.
 *
 * This returns the new page content to replace the page content made obsolete
 * by the form submission.
 *
 * @see field_add_more_submit()
 */
function dynamic_panes_ipe_sort_sort_js($form, $form_state) {
  ctools_include('ajax');

  $commands = array();
  $commands[] = ctools_ajax_command_reload();
  return array('#type' => 'ajax', '#commands' => $commands);
}

/**
 * Add a button to the IPE toolbar.
 */
function dynamic_panes_ipe_toolbar_add_button($cache_key, $id, $button) {
  $buttons = &drupal_static('dynamic_panes_ipe_toolbar_buttons', array());
  $buttons[$cache_key][$id] = $button;
}
