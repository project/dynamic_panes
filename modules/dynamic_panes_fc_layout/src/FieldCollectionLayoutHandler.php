<?php

/**
 * @file
 * Contains class for Field Collection layout handler.
 */

namespace Drupal\dynamic_panes_fc_layout;

use Drupal\dynamic_panes\LayoutHandler;

/**
 * Class for Field Collection layout handler.
 */
class FieldCollectionLayoutHandler extends LayoutHandler {}
