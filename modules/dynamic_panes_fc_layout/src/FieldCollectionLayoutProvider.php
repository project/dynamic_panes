<?php

/**
 * @file
 * Contains class for Field Collection layout provider.
 */

namespace Drupal\dynamic_panes_fc_layout;

use Drupal\dynamic_panes\LayoutProvider;

/**
 * Field Collection layout provider class.
 */
class FieldCollectionLayoutProvider extends LayoutProvider {}
